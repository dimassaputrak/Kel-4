[![build status](https://gitlab.com/PPL2018csui/Kelas-A/Kel-4/badges/sit_uat/build.svg)](https://gitlab.com/PPL2018csui/Kelas-A/Kel-4/tree/sit_uat)
[![codecov](https://codecov.io/gl/Kelas-A/Kel-4/branch/sit_uat/graph/badge.svg)](https://codecov.io/gl/Kelas-A/Kel-4/branch/sit_uat)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

>
## Dekowarehouse project v.1.0
On Tuesday, April 24th 2018 <br />
List of Feature :
- View list of assets
- Quickview on list item
- View detail of asset
- Download asset
- View list of assets by category
- Compare two asset
- Pagination
