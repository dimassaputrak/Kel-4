import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import CategoryList from '../../components/categoryList/categoryList';
import './css/allCategory.css';

class AllCategory extends Component {
	render() {
		return (
			<BrowserRouter>
				<div className='AllCategory'>
					<Header />
					<div>
						<h3> List Of Category </h3>
						<CategoryList />
					</div>
					<Footer />
				</div>
			</BrowserRouter>
		);
	}
}

export default AllCategory;