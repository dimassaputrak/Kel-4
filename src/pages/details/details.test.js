import React from 'react';
import ReactDOM from 'react-dom';
import Detail from './details';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<Detail />, div);
	ReactDOM.unmountComponentAtNode(div);
});