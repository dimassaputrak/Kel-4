import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import Slider from '../../components/slider/slider';
import Description from '../../components/description/description';
import Specification from '../../components/specification/specification';
import DownloadSatuanButton from '../../components/downloadSatuanButton/downloadSatuanButton';
import CompareButton from '../../components/compareButton/compareButton';
import AddToCollectionButton from '../../components/addToCollectionButton/addToCollectionButton';
import './css/details.css';

class Detail extends Component {
	render() {
		return (
			<BrowserRouter>
				<div className="Detail">
					<Header />
					<div className="flex-container">
						<div className="left-column">
							<Slider id={this.props.id}/>
							<Description id={this.props.id}/>
						</div>
						<div className="right-column">
							<table>
								<tbody>
									<tr>
										<td><DownloadSatuanButton id={this.props.id}/></td>
										<td><CompareButton id={this.props.id} category={this.props.category}/></td>
									</tr>
									<tr>
										<td colSpan="2"><AddToCollectionButton id={this.props.id} /></td>
									</tr>
								</tbody>
							</table>
							<Specification id={this.props.id}/>
						</div>
					</div>
					<Footer />
				</div>
			</BrowserRouter>
		);
	}
}
export default Detail;