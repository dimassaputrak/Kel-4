import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import TableListByCategory from '../../components/tableListByCategory/tableListByCategory';
import './css/categoryList.css';

class CategoryList extends Component {

	render() {
		return (
			<BrowserRouter>
				<div className='CategoryList'>
					<Header />
					<h1> List of Assets </h1>
					<TableListByCategory category={this.props.category}/>
					<Footer />
				</div>
			</BrowserRouter>	
		);
	}
}

export default CategoryList;