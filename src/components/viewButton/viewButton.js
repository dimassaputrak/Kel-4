import React, { Component } from 'react';
import { BrowserRouter, Link } from 'react-router-dom';
import './css/viewButton.css';

class ViewButton extends Component {
	render() {
		var link = '/details/'+ this.props.category+ '/' + this.props.id;
		return (
			<BrowserRouter>
				<div className='ViewButton'>
					<Link to={link} onClick='window.location.reload();'>
						<button>
							View Detail
						</button>
					</Link>
				</div>
			</BrowserRouter>
		);
	}
}
export default ViewButton;