import React from 'react';
import ReactDOM from 'react-dom';
import ViewButton from './viewButton.js';


it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<ViewButton />, div);
	ReactDOM.unmountComponentAtNode(div);
});
