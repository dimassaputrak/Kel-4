import React, { Component } from 'react';
import JSZip from 'jszip';
import JSZipUtils from 'jszip-utils';
import FileSaver from 'file-saver';
import './css/downloadMultipleButton.css';

class downloadMultipleButton extends Component {
	/* istanbul ignore next */
	downloadAll(){
		/* istanbul ignore next */
		var zip = new JSZip();
		/* istanbul ignore next */
		var count = 0;
		/* istanbul ignore next */
		var zipFilename = 'MyCollections.zip';
		/* istanbul ignore next */
		var urls = JSON.parse(window.localStorage.getItem('urlCollections'));
		/* istanbul ignore next */
		urls.forEach(function(url){
		  	// loading a file and add it in a zip file
		  	/* istanbul ignore next */
		  	var filename = url.split('/');
		  	/* istanbul ignore next */
		  	JSZipUtils.getBinaryContent(url, function (err, data) {
		     	/* istanbul ignore next */
		     	if(err) {
		        	throw err; // or handle the error
		     	}
		     	/* istanbul ignore next */
		     	zip.file(filename[filename.length-1], data, {binary:true});
		     	/* istanbul ignore next */
		     	count++;
		     	/* istanbul ignore next */
			  	if (count === urls.length) {
					zip.generateAsync({type:'blob'}).then(function(content) {
						FileSaver.saveAs(content, zipFilename);
					});
				}
		  	});

		});

	}

	render() {
		return (
			<div className="downloadMultipleButton">
				<a className="downloadMultiple-button" onClick={this.downloadAll}>
					DOWNLOAD
				</a>
			</div>
		);
	}
}
export default downloadMultipleButton;