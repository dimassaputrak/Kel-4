import React from 'react';
import ReactDOM from 'react-dom';
import DownloadMultipleButton from './downloadMultipleButton';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<DownloadMultipleButton />, div);
	ReactDOM.unmountComponentAtNode(div);
});
