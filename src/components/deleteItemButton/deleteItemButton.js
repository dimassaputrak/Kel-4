import React, { Component } from 'react';

class deleteItemButton extends Component {
	/* istanbul ignore next */
	deleteItem(){
		var id = parseInt(this.props.id, 10);
		var collections = JSON.parse(window.localStorage.getItem('collections'));
		var urlCollections = JSON.parse(window.localStorage.getItem('urlCollections'));
		if(collections.includes(id)){
			var deletedItemIndex = collections.indexOf(id);
			var deletedUrlIndex = collections.indexOf(id);
			collections.splice(deletedItemIndex,1);			
			urlCollections.splice(deletedUrlIndex,1);			
		}
		window.localStorage.setItem('collections', JSON.stringify(collections));
		window.localStorage.setItem('urlCollections', JSON.stringify(urlCollections));
		alert("Item has been deleted from Collection");
		window.location.reload();
	}
	render() {
		return (
			<div className="deleteItemButton">
				<a className="deleteItem-button" onClick={this.deleteItem.bind(this)} >
					Delete
				</a>
			</div>
		);
	}
}
export default deleteItemButton;