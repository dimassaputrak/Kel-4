import React from 'react';
import ReactDOM from 'react-dom';
import DeleteItemButton from './deleteItemButton.js';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<DeleteItemButton />, div);
	ReactDOM.unmountComponentAtNode(div);
});
