import React, { Component } from 'react';
import { InstantSearch, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import Download from 'downloadjs';
import './css/downloadSatuanButton.css';

class DownloadSatuanButton extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: 1,
		};
		this.url = '';
	}
	/* istanbul ignore next */
	handleOnClick() {
		const link = this.url;
		Download(link);
	}

	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<div>
				{hits.map((hit) => {
					let image = '';
					image = hit.image;
					if(image === ''){
						image = 'http://f1-styx.imgix.net/catalogue/OLH-321634_1.jpg?w=960&h=480&fit=clamp&auto=format';
					};
					var res = image.split('://');
					this.url = 'https://'+res[1];
					return(
						<button key={hit.id} className="download-satuan-button" onClick={this.handleOnClick.bind(this)}>Download</button>
					);
				}
				)}
			</div>
		);
  		/* istanbul ignore next */
		const ConnectedRender = connectHits(RenderHits);
		/* istanbul ignore next */
		const Content = ({ ids }) => (
			<Index indexName="staging_products">
				<Configure filters={`id=${ids}`}/>
				<ConnectedRender />
			</Index>
		);

		return (
			<InstantSearch 
				appId='D2VY06YP2A' 
				apiKey='6014c0a86a26290fa9e3654153a74db4' 
				indexName='staging_products'
			>
				<div className="DownloadSatuanButton">
					<Content ids={this.props.id}/>    
				</div>
			</InstantSearch>
		);
	}
}

export default DownloadSatuanButton;