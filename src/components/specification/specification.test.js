import React from 'react';
import ReactDOM from 'react-dom';
import Specification from './specification';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';


describe('Specification renders a Specification based on id', () => {
	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(<Specification id='1' />, div);
		ReactDOM.unmountComponentAtNode(div);
	});
	it('should match its snapshot', () => {
		const wrapper = renderer.create(<Specification id='1' />).toJSON();
		expect(wrapper).toMatchSnapshot();
  	});
});