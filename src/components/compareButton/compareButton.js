import React, { Component } from 'react';
import { BrowserRouter, Link } from 'react-router-dom';
import './css/compareButton.css';

class CompareButton extends Component {
	render() {
		var link = '/comparison/'+ this.props.category+'/'+ this.props.id;
		return (
			<BrowserRouter>
				<div className="CompareButton">
					<Link className="compare-link" to={link} onClick='window.location.reload();'>
						<button className="compare-button">
							Compare
						</button>
					</Link>
				</div>
			</BrowserRouter>	
		);
	}
}
export default CompareButton;