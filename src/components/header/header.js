import React, { Component } from 'react';
import logo from './assets/dekowarehouse_white.png';
import home from './assets/home.svg';
import searchIcon from './assets/magnifying-glass.png';
import { BrowserRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import CollectionPopup from '../../components/collectionPopup/collectionPopup';
import './css/header.css';

class Header extends Component {
	static contextTypes = {
    	router: PropTypes.object
  	}

	constructor(props, context) {
	    super(props, context);
	    this.state = {value: ''};
	    this.handleChange = this.handleChange.bind(this);
	    this.handleSubmit = this.handleSubmit.bind(this);
	}
	/* istanbul ignore next */
	handleChange(event) {
	    this.setState({value: event.target.value});
	}
	/* istanbul ignore next */
	handleSubmit(event) {
		this.context.router.history.push('/search?query='+this.state.value);
		window.location.reload();
	    event.preventDefault();
	}
	render() {
		return (
			<div className="Header">
				<div className="navbar">
					<ul className="nav">
						<form onSubmit={this.handleSubmit}>
							<li className="itemSearch">
								<div>
						    		<BrowserRouter><Link to='/search' onClick='window.location.reload();'>
										<button><img src={searchIcon} alt="search-icon" width="18px" height="18px" /></button>
									</Link></BrowserRouter>
						    	</div>
								<div>
									<input type="text" placeholder="Search products here..." name="search" value={this.state.value} onChange={this.handleChange} />
								</div>
							</li>	
							<li className="item">
								<BrowserRouter><Link to='/' onClick='window.location.reload();'>
									<img src={home} alt='HOME' height='30px' />
								</Link></BrowserRouter>						
							</li>
							<li className="item">
								<CollectionPopup />
							</li>
						</form>
					</ul>
					<div className="logo">
						<img src={logo} alt="logo"/>
					</div>
				</div>
				<hr />
			</div>
		);
	}
}

Header.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired,
	}),
};

export default Header;
