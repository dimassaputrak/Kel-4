import React, { Component } from 'react';
import { InstantSearch, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import { BrowserRouter } from 'react-router-dom';
import DownloadMultipleButton from '../../components/downloadMultipleButton/downloadMultipleButton';
import DeleteItemButton from '../../components/deleteItemButton/deleteItemButton';
import './css/collectionPopup.css';
import close from './assets/close.png';
import collection2 from './assets/collection2.png';

class CollectionPopup extends Component {
	/* istanbul ignore next */
	clearCollection(){
		let emptyCollection = [];
		window.localStorage.setItem('collections', JSON.stringify(emptyCollection));
		window.localStorage.setItem('urlCollections', JSON.stringify(emptyCollection));
		// alert("Items have been cleared from Collection");
		window.location.reload();
	}
	/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
	/* istanbul ignore next */
	openNav() {
    	document.getElementById('mySidenav').style.width = '350px';
	    document.getElementById('root').style.marginRight = '350px';
	}

	/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
	/* istanbul ignore next */
	closeNav() {
	    document.getElementById('mySidenav').style.width = '0';
	    document.getElementById('root').style.marginRight = '0';
	}

	getQueryString( collectionArray ){
		let queryString = '';
		/* istanbul ignore next */
		if(collectionArray !== undefined && collectionArray.length > 0){
			for(let i = 0; i < collectionArray.length; i++){
				queryString += `id=${collectionArray[i]} OR `;
			}
			queryString = queryString.substring(0, queryString.length-3);			
		} else {
			queryString = `id=${0}`;			
		}
		return queryString;
	}
	render() {
		/* istanbul ignore next */
		if(typeof(Storage) !== "undefined"){
			var collections, urlCollections;
			/* istanbul ignore next */
			if(window.localStorage.collections === undefined || window.localStorage.urlCollections === undefined){
				collections = [];
				urlCollections = [];
			} 
			/* istanbul ignore next */
			else {
				collections = JSON.parse(window.localStorage.getItem('collections'));
				urlCollections = JSON.parse(window.localStorage.getItem('urlCollections'));
			}
			/* istanbul ignore next */
			window.localStorage.setItem('collections', JSON.stringify(collections));
			/* istanbul ignore next */
			window.localStorage.setItem('urlCollections', JSON.stringify(urlCollections));
		}
		// alert(collections);
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<table>
				<tbody>
					{hits.map(( hit ) => {
						let itemImage = hit.image;
						let itemName = hit.title;
						let itemId = hit.id;
						return(
							<tr key={hit.id}>
								<td>
									<img src={itemImage}  alt="Item" width="50"/>	
								</td>
								<td>
									<p className='listCollectedItem' >
										{itemName}
									</p>
								</td>
								<td>
									<DeleteItemButton id={itemId} url={itemImage} />
								</td>
							</tr>
						);
					}
					)}
				</tbody>
			</table>
		);
		const ConnectedRender = connectHits(RenderHits);

		const Content = ({ ids }) => {
			return (
		   	(
		    	<Index indexName='staging_products'>
		    		<Configure filters={this.getQueryString(ids)}/>
		    		<ConnectedRender />
		       </Index>
				)
		    );
		};

		return (
			<InstantSearch 
		        appId='D2VY06YP2A' 
		        apiKey='6014c0a86a26290fa9e3654153a74db4' 
		        indexName='staging_products'
		    >
				<BrowserRouter>
					<div className='popupCollection'>	
						<a className='showCollectionButton' id='collectionFont' onClick={this.openNav} >COLLECTION </a>
						<a className='showCollectionButton' id='collectionim' onClick={this.openNav} >
							<img src={collection2} height='42px' alt='COLLECTION' />
						</a>				
						<div id='mySidenav' className='sidenav'>
							<p className='close-button' onClick={this.closeNav}>
								<img src = {close} width='30px' alt='close'/>
							</p>
							<div className='titleModal'>
								<h4>Collected Item</h4>
							</div>
							<hr />
							<br />
							<div className='bodyModalCollection'>
								<Content ids={collections} />
							</div>
							<div className="modal-footer">
								<table>
									<tbody>
										<tr>
											<td>
												<div className="clearCollectionButton">
													<a className="clearCollection-button" onClick={this.clearCollection}>CLEAR</a>
												</div>
											</td>
											<td><DownloadMultipleButton /></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</BrowserRouter>
			</InstantSearch>
		);
	}
}

export default CollectionPopup;