import React, { Component } from 'react';
import { BrowserRouter, Link } from 'react-router-dom';
import './css/errorPage.css';

class ErrorBoundary extends Component {
	render() {
		var dataSize = this.props.dataSize;
		var locationPage = this.props.location;
		if(dataSize < 1 ){
			return (
				<div className='property'>
					<div className="container">
						<div className="errorPage">
							<p><b>We're sorry, the page you are looking for doesn't exist or an other error occured</b></p>
							<p> One of the following options may help in locating the page you requested:</p>
							<ul>
								<li>Go back</li>
								<li>Check the URL in the address bar of your browser for possible misspellings.</li>
								<li>Head over to 
									<BrowserRouter><Link to='/' onClick='window.location.reload();'>
										<button>
											dekowarehouse.com
										</button>
									</Link></BrowserRouter>	
								 	to choose a new direction
								</li>
							</ul>
						</div>
					</div>
				</div>
			);
		} if(locationPage!== undefined && locationPage.indexOf('/404') >= 0){
			return (
				<div className='property'>
					<div className="container">
						<div className="errorPage">
							<p><b>We're sorry, the page you are looking for doesn't exist or an other error occured</b></p>
							<p> One of the following options may help in locating the page you requested:</p>
							<ul>
								<li>Go back</li>
								<li>Check the URL in the address bar of your browser for possible misspellings.</li>
								<li>Head over to 
									<BrowserRouter><Link to='/' onClick='window.location.reload();'>
										<button> dekowarehouse.com </button>
									</Link></BrowserRouter>	
								 	 to choose a new direction
								</li>
							</ul>
						</div>
					</div>
				</div>
			);
		}
		return(
			<div className='property'>{this.props.children}</div>
		);
	}
}

export default ErrorBoundary;