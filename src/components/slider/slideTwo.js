import React, { Component } from 'react';
import { InstantSearch, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import './css/slider.css';

class SlideTwo extends Component {
	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<div>
				{hits.map((hit) => {
					let image = '';
					image = hit.additionalImages[0];
					if(image === ''){
						image = 'https://f1-styx.imgix.net/catalogue/OLH-321634_1.jpg?w=960&h=480&fit=clamp&auto=format';
					};
					return(
						<div><img src={image} alt='slider2' /></div>
					);
				}
			 )}
			</div>
		);
		/* istanbul ignore next */
		const ConnectedRender = connectHits(RenderHits);
		/* istanbul ignore next */
		const Content = ({ ids }) => (
			<Index indexName="staging_products">
				<Configure filters={`id=${ids}`}/>
				<ConnectedRender />
			</Index>
		);

		return (
			<InstantSearch 
				appId='D2VY06YP2A' 
				apiKey='6014c0a86a26290fa9e3654153a74db4' 
				indexName='staging_products'
			>
				<div className="slide3">
					<Content ids={this.props.id}/>				
				</div>
			</InstantSearch>
		);
	}
}

export default SlideTwo;