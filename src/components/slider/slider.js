import React, { Component } from 'react';
import SlideOne from './slideOne';
import SlideTwo from './slideTwo';
import SlideThree from './slideThree';
import RightArrow from './rightArrow';
import LeftArrow from './leftArrow';
import './css/slider.css';

export default class Slider extends Component {
	constructor(props) {
		super(props);

		this.state = {
			slidecount: 1
		};

		this.nextSlide = this.nextSlide.bind(this);
		this.previousSlide = this.previousSlide.bind(this);
	}

	nextSlide() {
		this.setState({ slidecount: this.state.slidecount + 1 });
		if(this.state.slidecount === 3) {
			this.setState({ slidecount: 1 });
		}
	}

	previousSlide() {
		this.setState({ slidecount: this.state.slidecount - 1 });
		if(this.state.slidecount <= 1) {
		   this.setState({ slidecount: 3 });
		}
	}

	render() {
		var id = this.props.id;
		return (
			<div className="Slider">
				<div className = "leftSliderButton">
					<LeftArrow previousSlide={this.previousSlide} slidecount={this.state.slidecount}/>
				</div>
				<div className = "sliderImages" slidecount={this.state.slidecount}>
					{ this.state.slidecount === 1 ? <SlideOne id={id}/> : null }
					{ this.state.slidecount === 2 ? <SlideTwo id={id}/> : null }
					{ this.state.slidecount === 3 ? <SlideThree id={id}/> : null }
				</div>
				<div className = "rightSliderButton">
					<RightArrow nextSlide={this.nextSlide} slidecount={this.state.slidecount} />
				</div>
			</div>
		);
	}
}
