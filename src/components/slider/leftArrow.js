import React from 'react';
import './css/slider.css';

const LeftArrow = (props) => {
	return (
		<div onClick={props.previousSlide} className="backArrow">
			<button className="left-arrow" aria-hidden="true">&lt;</button>
		</div>
	);
};

export default LeftArrow;