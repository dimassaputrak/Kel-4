import React from 'react';
import ReactDOM from 'react-dom';
import SlideTwo from './slideTwo';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';

describe('SlideTwo renders an image based on id', () => {
	it('should match its snapshot', () => {
		const wrapper = renderer.create(<SlideTwo id='1' />).toJSON();
		expect(wrapper).toMatchSnapshot();
  	});
});

// it('should return alt image when image not found', () => {
// 	const wrapper = shallow(<SlideTwo id='40997'/>);
// 	const slide = wrapper.find('[className="slide2"]').first(); 
// 	const image = wrapper.find('img');

// 	expect(image.prop('src')).toEqual('https://f1-styx.imgix.net/catalogue/OLH-321634_1.jpg?w=960&h=480&fit=clamp&auto=format');
// });