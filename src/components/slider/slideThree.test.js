import React from 'react';
import ReactDOM from 'react-dom';
import SlideThree from './slideThree';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';

describe('SlideThree renders an image based on id', () => {
	it('should match its snapshot', () => {
		const wrapper = renderer.create(<SlideThree id='1' />).toJSON();
		expect(wrapper).toMatchSnapshot();
  	});
});

// it('should return alt image when image not found', () => {
// 	const wrapper = shallow(<SlideThree id='29237'/>);
// 	const slide = wrapper.find('[className="slide3"]').first(); 
// 	const image = wrapper.find('img');

// 	expect(image.prop('src')).toEqual('https://f1-styx.imgix.net/catalogue/OLH-321634_1.jpg?w=960&h=480&fit=clamp&auto=format');
// });