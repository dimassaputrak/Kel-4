import React from 'react';
import ReactDOM from 'react-dom';
import SlideOne from './slideOne';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';

describe('SlideOne renders an image based on id', () => {
	it('should match its snapshot', () => {
		const wrapper = renderer.create(<SlideOne id='1' />).toJSON();
		expect(wrapper).toMatchSnapshot();
  	});
});