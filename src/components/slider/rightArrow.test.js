import React from 'react';
import ReactDOM from 'react-dom';
import RightArrow from './rightArrow';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<RightArrow />, div);
	ReactDOM.unmountComponentAtNode(div);
});