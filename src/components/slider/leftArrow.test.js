import React from 'react';
import ReactDOM from 'react-dom';
import LeftArrow from './leftArrow';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<LeftArrow />, div);
	ReactDOM.unmountComponentAtNode(div);
});
