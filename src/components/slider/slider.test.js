import React from 'react';
import ReactDOM from 'react-dom';
import Slider from './slider';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';
import RightArrow from './rightArrow';
import LeftArrow from './leftArrow';

it('should go to next slide when clicked', () => {
	const wrap = shallow(<Slider />);
	const wrapper = shallow(<RightArrow />);
	const nextButton = wrapper.find('div');

	const currentSlideCount = wrap.find('[className="sliderImages"]').props().slidecount;
	nextButton.simulate('click');
	const nextSlideCount = wrap.find('[className="sliderImages"]').props().slidecount;
 
	expect(currentSlideCount).toEqual(nextSlideCount);
});

it('should go to prev slide when clicked', () => {
	const wrap = shallow(<Slider />);
	const wrapper = shallow(<LeftArrow />);
	const prevButton = wrapper.find('div');

	const currentSlideCount = wrap.find('[className="sliderImages"]').props().slidecount;
	prevButton.simulate('click');
	const prevSlideCount = wrap.find('[className="sliderImages"]').props().slidecount;
 
	expect(currentSlideCount).toEqual(prevSlideCount);
});

it('should go to the next slide', () => {
	const wrapper = shallow(<Slider id='1'/>);
	wrapper.instance().nextSlide();
	expect(wrapper.instance().state.slidecount).toEqual(2);
});

it('should go to the next slide', () => {
	const wrapper = shallow(<Slider id='1'/>);
	wrapper.instance().nextSlide();
	wrapper.instance().nextSlide();
	expect(wrapper.instance().state.slidecount).toEqual(1);
});

it('should go to the previous slide', () => {
	const wrapper = shallow(<Slider id='1'/>);
	wrapper.instance().previousSlide();
	expect(wrapper.instance().state.slidecount).toEqual(3);
});

it('should go to the previous slide', () => {
	const wrapper = shallow(<Slider id='1'/>);
	wrapper.instance().previousSlide();
	wrapper.instance().previousSlide();
	expect(wrapper.instance().state.slidecount).toEqual(2);
});
