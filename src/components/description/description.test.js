import React from 'react';
import ReactDOM from 'react-dom';
import Description from './description';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';


describe('Description renders a description based on id', () => {
	it('should match its snapshot', () => {
		const wrapper = renderer.create(<Description id='1' />).toJSON();
		expect(wrapper).toMatchSnapshot();
  	});
  	it('show no description available when there is no data', () => {
		const wrapper = renderer.create(<Description id='12' />).toJSON();
		expect(wrapper).toMatchSnapshot();
	});
});
