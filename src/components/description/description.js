import React, { Component } from 'react';
import { InstantSearch, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import { BrowserRouter } from 'react-router-dom';
import './css/description.css';

class Description extends Component {
	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<div>{hits.map(hit => <div key={hit.id}>{hit.description}</div>)}</div>
		);
		/* istanbul ignore next */
		const ConnectedRender = connectHits(RenderHits);
		/* istanbul ignore next */
		const Content = ({ ids }) => (
		    <Index indexName="staging_products">
		        <Configure filters={`id=${ids}`}/>
		        <ConnectedRender />
		    </Index>
		);
  		
  		return (
			<InstantSearch 
		        appId="D2VY06YP2A" 
		        apiKey="6014c0a86a26290fa9e3654153a74db4" 
		        indexName="staging_products"
		    >
				<BrowserRouter>
					<div className="description">
						<div className="descriptionTitle">
							<h3>Description</h3>
						</div>
						<div className="descriptionText">
							<Content ids={this.props.id}/>
						</div>
					</div>
				</BrowserRouter>
			</InstantSearch>
		);
	}
}

export default Description;