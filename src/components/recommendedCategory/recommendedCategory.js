import React, { Component } from 'react';
import { BrowserRouter, Link } from 'react-router-dom';
import ErrorBoundary from '../errorPage/errorPage';
import './css/recommendedCategory.css';

class RecommendedCategory extends Component {
	constructor() {
		super();
		this.state = {
			categories: [{cat : 'Kasur', url : 'https://f1-styx.imgix.net/icon/category/kasur-48.svg'},
				{cat : 'Furnitur', url : 'https://f1-styx.imgix.net/icon/category/furnitur-48.svg'}, 
				{cat : 'Kamar Tidur', url : 'https://f1-styx.imgix.net/icon/category/kamar-tidur-48.svg'}, 				 
				{cat : 'Dekor', url : 'https://f1-styx.imgix.net/icon/category/dekorasi-48.svg'}, 
				{cat : 'Lampu', url : 'https://f1-styx.imgix.net/icon/category/lampu-48.svg'}, 
				{cat : 'Kamar Mandi', url : 'https://f1-styx.imgix.net/icon/category/kamar-mandi-48.svg'}, 
				{cat : 'Home Improvement', url : 'https://f1-styx.imgix.net/icon/category/home-improvement-48.svg'}, 
				{cat : 'Dapur & Ruang Makan', url : 'https://f1-styx.imgix.net/icon/category/dapur-&-ruang-makan-48.svg'}]
		};
	}
	render() {
		let category;
  		/* istanbul ignore next */
		if (this.state.categories.length) {
		  category = this.state.categories.map(
		    (obj) => (
		    	<li className='ais-Hits-item' key={obj.cat}>
		    		<Link to={'items/'+obj.cat} onClick='window.location.reload();'>
							<div className='item'>
								<img src={obj.url} alt='asset' width='100px'></img>
							</div>
							<div className='item' id='k+=1'>
								<h5><b>{obj.cat}</b></h5>
							</div>
						</Link>
					</li>
				)
		  	);
		} 
		/* istanbul ignore next */
		else {
		  category = <h1>Working</h1>;
		}

		return (
			<ErrorBoundary>
				<BrowserRouter>
					<div className='RecommendedCategory'>
						<h4> Category </h4>
						<div className='ais-MultiIndex__root'>
							<div className='ais-Hits'>
								<ul className='ais-Hits-list'>
									{category}
								</ul>
							</div>
						</div>
						<br />
					</div>
				</BrowserRouter>
			</ErrorBoundary>
		);
	}
}

export default RecommendedCategory;