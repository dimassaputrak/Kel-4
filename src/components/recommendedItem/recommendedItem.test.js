import React from 'react';
import ReactDOM from 'react-dom';
import RecommendedItem from './recommendedItem';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<RecommendedItem />, div);
	ReactDOM.unmountComponentAtNode(div);
});
it('gets the local database', () => {
	const db = require('../../file_source/dummy_database.json');
	if(!db){
		return 'undefined';
	} 
});