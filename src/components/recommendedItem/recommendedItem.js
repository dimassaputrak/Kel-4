import React, { Component } from 'react';
import './css/recommendedItem.css';
import QuickView from '../quickView/quickView';
import ViewButton from '../viewButton/viewButton.js';
import { InstantSearch, Hits, Pagination } from 'react-instantsearch/dom';

/* istanbul ignore next */
const Hit = ({hit}) =>
	<div className='recommendedItemText'>
		<div className='item'>
			<img src={hit.image} alt='asset'/>
		</div>
		<div className='button'>
			<QuickView id={hit.id} category={hit.hcategories.lvl0} name={hit.title} image_general={hit.image} description = {hit.description} />
			<ViewButton id={hit.id} category={hit.hcategories.lvl0} />
		</div>
		<hr />
		<div className='item'>
			<p id={hit.id}><b>{hit.title}</b></p>				
		</div>
	</div>;

const Content = () =>
	<div>
		<Hits hitComponent={Hit} />
		<div className='pagination'>
			<br />
			<Pagination showLast padding={2} />
		</div>
	</div>;

class RecommendedItem extends Component{
	render() {
		console.log(this.props);
		return (
			<InstantSearch 
		        appId="D2VY06YP2A" 
		        apiKey="6014c0a86a26290fa9e3654153a74db4" 
		        indexName="staging_products"
		    >
		    	<div className='recommendedItem'>
		    		<h4> Recommended Item </h4>
					<section className='List-item'>
						<div>
							<Content />
						</div>
					</section>
				</div>
		    </InstantSearch>
		);
	}
}

export default RecommendedItem;