import React, { Component } from 'react';
import { InstantSearch, Configure, Index } from 'react-instantsearch/dom';
import { connectHits } from 'react-instantsearch/connectors';
import './css/addToCollectionButton.css';

class addToCollectionButton extends Component {
	/* istanbul ignore next */
	addToCollection(){
		/* istanbul ignore next */
		var id = parseInt(this.props.id, 10);
		/* istanbul ignore next */
		var imageUrl = document.getElementById('addToCollectionButtonId').getAttribute('itemimage');
		/* istanbul ignore next */
		var res = imageUrl.split('://');
		/* istanbul ignore next */
		var url_res = 'https://'+res[1];
		/* istanbul ignore next */
		var collections = JSON.parse(window.localStorage.getItem('collections'));
		/* istanbul ignore next */
		var urlCollections = JSON.parse(window.localStorage.getItem('urlCollections'));
		/* istanbul ignore next */
		if(!collections.includes(id)){
			collections.push(id);
			urlCollections.push(url_res);	
		}
		/* istanbul ignore next */
		window.localStorage.setItem('collections', JSON.stringify(collections));
		/* istanbul ignore next */
		window.localStorage.setItem('urlCollections', JSON.stringify(urlCollections));
		/* istanbul ignore next */
		alert("Item has been added to Collection");
		/* istanbul ignore next */
		window.location.reload();
	}
	render() {
		/* istanbul ignore next */
		const RenderHits = ({ hits }) => (
			<div>
				{hits.map(( hit ) => {
					let itemimage = hit.image;
					return(
						<button key={hit.id} className="addToCollection-button" id="addToCollectionButtonId" itemimage={itemimage} onClick={this.addToCollection.bind(this)} >
							Add To Collection
						</button>
					);
				}
				)}
			</div>
		);
		const ConnectedRender = connectHits(RenderHits);

		const Content = ({ ids }) => (
		    <Index indexName="staging_products">
		        <Configure filters={`id=${ids}`}/>
		        <ConnectedRender />
		    </Index>
		);
  		return (
			<InstantSearch 
		        appId="D2VY06YP2A" 
		        apiKey="6014c0a86a26290fa9e3654153a74db4" 
		        indexName="staging_products"
		    >
				<div className="addToCollectionButton">
					<Content ids={this.props.id}/>
				</div>
			</InstantSearch>
		);
	}
}
export default addToCollectionButton;