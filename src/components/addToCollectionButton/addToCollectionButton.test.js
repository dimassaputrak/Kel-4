import React from 'react';
import ReactDOM from 'react-dom';
import AddToCollectionButton from './addToCollectionButton.js';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<AddToCollectionButton />, div);
	ReactDOM.unmountComponentAtNode(div);
});
