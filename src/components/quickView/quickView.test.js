import React from 'react';
import ReactDOM from 'react-dom';
import QuickView from './quickView';
import { configure,shallow } from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import toJson from 'enzyme-to-json';
import renderer from 'react-test-renderer';

it('should toggle second button’s toggle state when clicking on first button', () => {
	const wrapper = shallow(<QuickView id='1'/>);
	const firstButton = wrapper.find('button'); 
	const modalButton = wrapper.find('[className="modal"]').first();
	const secondButton = wrapper.find('[className="btn"]');

	firstButton.simulate('click');
	expect(modalButton.props().toggle).toEqual('false');
});

