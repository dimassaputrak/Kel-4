import React, { Component } from 'react';
import './css/table.css';
import dummy_database from '../../file_source/dummy_database.json';
import QuickView from '../quickView/quickView';
import ViewButton from '../viewButton/viewButton.js';

class Tableasset extends Component {
	render() {
		var dbKeys = Object.keys(dummy_database.products);
		var allNames = dbKeys.map((t) =>
			<tr key={t}>
				<td><img src={dummy_database.products[t].image_general} alt='asset' width='200px'/></td>
				<td id={dummy_database.products[t].id}>{dummy_database.products[t].name}</td>
				<td>
					<span>
						<QuickView id={dummy_database.products[t].id} name={dummy_database.products[t].name} image_general={dummy_database.products[t].image_general} description = {dummy_database.products[t].description} category={dummy_database.products[t].category}/>
						<ViewButton id={dummy_database.products[t].id} category={dummy_database.products[t].category} />
					</span>
				</td>
			</tr> 
		);

		return (
			<div className='Home-pg'>
				<h1 className='list-header'>List Assets</h1>

				<section className='List-item'>
					<div>
						<table cellPadding='0' cellSpacing='0'>
							<thead>
								<tr>
									<th>Photo</th>
									<th>Assets</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								{allNames}
							</tbody>
						</table>
					</div>
				</section>
			</div>
		);
	}
}

export default Tableasset;