import React from 'react';
import ReactDOM from 'react-dom';
import Tabelasset from './table';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<Tabelasset />, div);
	ReactDOM.unmountComponentAtNode(div);
});

it('gets the local database', () => {
	const db = require('../../file_source/dummy_database.json');
	if(!db){
		return 'undefined';
	} 
});