import React from 'react';
import ReactDOM from 'react-dom';
import ComparedPlaceHolder from './comparedPlaceHolder';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<ComparedPlaceHolder />, div);
	ReactDOM.unmountComponentAtNode(div);
});
