import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CategoryList from './pages/categoryList/categoryList';
import Home from './pages/home/home';
import Detail from './pages/details/details';
import Comparison from './pages/comparison/comparison';
import ErrorPage from './pages/errorPage/errorPage.js';
import ResultPage from './pages/resultPage/resultPage.js';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
	<BrowserRouter>
		<Switch>
			<Route exact path='/' component={Home}/>
			<Route exact path='/404' component={ErrorPage}/>
			<Route path='/search' component={ResultPage}/>
			<Route exact path='/details/:category/:id' render={({ match }) => 
				<Detail id={match.params.id} category={match.params.category}/>} />
			<Route exact path='/comparison/:category/:id' render={({ match }) => 
				<Comparison id={match.params.id} category={match.params.category} />} />
			<Route exact path='/items/:category' render={({ match }) => 
				<CategoryList category={match.params.category}/>} />
			<Route exact path='/items/details/:id' render={({ match }) => 
				<Detail id={match.params.id}/>} />
			<Route exact path='/comparison/:id' render={({ match }) => 
				<Comparison id={match.params.id}/>} />
			<Redirect to='/404' />
		</Switch>
	</BrowserRouter>
), document.getElementById('root'));

export default 
registerServiceWorker();
